﻿using System;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace Twilio_Number
{
    class Program 
    {
        static void Main(string[] args)
        {
            //Account Info
            const string accountSid = "ACf2abe01ccad336104a12710e1dfb3fa6";
            const string authToken = "64d61ed37ceab4af8d4c7418590765ad";
            //Intialize new Twilio Client
            TwilioClient.Init(accountSid, authToken);
            //Call function to get user information
            GetInfo();
        }

        static void GetInfo()
        {
            //Get Information for Number
            Console.WriteLine("Enter the number you'd like to send to (including the area code)...");
            var sendNumber = Console.ReadLine();
            if(sendNumber.Length < 10)
            {
                Console.Write("Please ensure you've entered a phone number in the proper format...\n");
                GetInfo();
            } else
            {
                //Get body of message
                Console.WriteLine("Enter the short message you would like to send...");
                var sendMessage = Console.ReadLine();
                SendText(sendNumber, sendMessage);
            }
        }

        static void SendText(String sendNumber, String sendMessage)
        {
            //Send message
            var message = MessageResource.Create(
                body: sendMessage,
                from: new Twilio.Types.PhoneNumber("+12058609482"),
                to: new Twilio.Types.PhoneNumber("+1" + sendNumber)
            );
            
            Console.WriteLine(message.Status);
        }
    }
}
